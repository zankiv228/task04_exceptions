package com.zankiv.exceptions;

public class App {
    public static void main(String[] args) {

        try (AutoClose autoClose = new AutoClose()) {

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
