package com.zankiv.exceptions;

public class AutoClose implements AutoCloseable {

    AutoClose() {

    }

    @Override
    public void close() throws Exception {
        throw new MyException();
    }
}
