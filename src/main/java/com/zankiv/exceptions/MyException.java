package com.zankiv.exceptions;

public class MyException extends Exception {
    private String message = "It's my exception";

    MyException() {
        System.out.println(message);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
